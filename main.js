// repeated functions
// LOCAL STORAGE FUNCTIONS
function getIssues() {
  return JSON.parse(localStorage.getItem('issues'));
}
function putIssues(dataToPut) {
  localStorage.setItem('issues', JSON.stringify(dataToPut));
}
// CLASS ADD REMOVE FUNCTIONS
function addValidationClass(n) {
  for (let i = 0; i <arguments.length; i++) {
    arguments[i].classList.add('is-invalid');
    arguments[i].previousElementSibling.classList.add('invalid-text');
  }
}
function removeValidationClass(n) {
  for (let i = 0; i <arguments.length; i++) {
    arguments[i].classList.remove('is-invalid');
    arguments[i].previousElementSibling.classList.remove('invalid-text');
  }
}

function fetchIssues() {
  const issues = getIssues();
  // select the location that we're going to place HTML
  const issuesList = document.getElementById('issuesList');
  // reset HTML
  issuesList.innerHTML = ``;
  // cycle through parsed issue array and assign data to temp variables
  for (let i =0; i < issues.length; i++) {
    let id = issues[i].id;
    let desc = issues[i].description;
    let severity = issues[i].severity;
    let assignedTo = issues[i].assignedTo;
    let status = issues[i].status;
    // conditional formatting to check if issue is closed or open
    if (status == 'Closed') {
      buttonStyle = 'success';
      statusButtonText = 'Reopen';
    } else {
      buttonStyle = 'warning';
      statusButtonText = 'Close';
    }
    // place variables into HTML
    issuesList.innerHTML += 
      `<div class="well" id="${id}"><h6>Issue ID: ${id}</h6><p><span class="label label-info">${status}</span></p><h3>${desc}</h3>
      <p><span class="glyphicon glyphicon-time"></span> ${severity} <span class="glyphicon glyphicon-user"></span> ${assignedTo}</p><a href="#" class="btn btn-${buttonStyle}">${statusButtonText}</a> <a href="#" class="btn btn-danger">Delete</a> <a href="#" class="btn btn-primary">Edit</a></div>`;
  }
}

// save issues
function saveIssue(e) {
  // obtain field values as variables
  const issueId = chance.guid();
  const issueDesc = document.getElementById('issueDescInput').value;
  const issueSeverity = document.getElementById('issueSeverityInput').value;
  const issueAssignedTo = document.getElementById('issueAssignedToInput').value;
  const issuesStatus = 'Open';
  // validation function
  if (issueDesc.length < 1 || issueAssignedTo.length < 1) {
    addValidationClass(document.getElementById('issueDescInput'),document.getElementById('issueAssignedToInput'));
    return;
  }

  // place variables into an issue object
  const issue = {
    id: issueId,
    description: issueDesc,
    severity: issueSeverity,
    assignedTo: issueAssignedTo,
    status: issuesStatus
  }
  //checking local storage
  //if there is no data in local ->
  if (localStorage.getItem('issues') === null) {
    // create empty array
    let issues = [];
    // push new issue to array
    issues.push(issue);
    //place into local storage as a string
    putIssues(issues);
  } else {
    // if there is some data
    // parse the current local storage into array
    let issues = JSON.parse(localStorage.getItem('issues'));
    // add the new issue to the array
    issues.push(issue);
    // place into local storage as a string
    putIssues(issues);;
  }
  // remove any validation classes added
  removeValidationClass(document.getElementById('issueDescInput'),document.getElementById('issueAssignedToInput'));
  // reset form
  document.getElementById('issueInputForm').reset();
  // populate page with current issues
  fetchIssues();
  // prevent page reloading as this is default submit behaviour
  e.preventDefault();
}

// close issue function
function setStatusClosed(id) {
  // retrieve local storage issues list
  const issues = getIssues();
  // cycle through list
  for(let i = 0; i < issues.length; i++ ) {
    // check list for an id that matches the element closed
    if (issues[i].id == id) {
      issues[i].status = `Closed`;
    }
  }
  // place amended list back into local storage
  putIssues(issues);;
  // update HTML on page
  fetchIssues();
}

// Reopen issue function
function setStatusOpen(id) {
  // retrieve local storage issues list
  const issues = getIssues();

  // cycle through list
  for(let i = 0; i < issues.length; i++ ) {
    // check list for an id that matches the element closed
    if (issues[i].id == id) {
      issues[i].status = `Open`;
    }
  }
  // place amended list back into local storage
  putIssues(issues);
  // update HTML on page
  fetchIssues();
}

// delete issues
function deleteIssue(id) {
  //obtain local storage issues list
  let issues = getIssues();
  // if issues array only has 1 entry
  if (issues.length <= 1) {
    issues = [];
  } else {
    // cycle through issue array to find current issue
    for(let i = 0; i < issues.length; i++) {
      if (issues[i].id == id) {
        // remove issues
        issues.splice(i,1);
      }
    }
  }
  // place data back in local storage
  putIssues(issues);
  //populate page
  fetchIssues();
}

function editIssue(id) {

}

// submit button event handler
document.getElementById('issueInputForm').addEventListener('submit', saveIssue);
document.getElementById('issuesList').addEventListener('click', function(e){
  if (e.target.outerText == 'Close') {
    setStatusClosed(e.target.parentNode.id);
  } else if (e.target.outerText == 'Reopen') {
    setStatusOpen(e.target.parentNode.id);
  } else if (e.target.outerText == 'Delete') {
    deleteIssue(e.target.parentNode.id);
  } else if (e.target.outerText == 'Edit') {
    editIssue(e.target.parentNode.id);
  }
})